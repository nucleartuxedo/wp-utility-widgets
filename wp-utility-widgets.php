<?php
/*
 * Plugin Name: WP Utility Widgets 
 * Description: Adds a widget type labeled "Utility Code Widget". Registers 3 new widget areas. A footer, a utility area that's placed in the footer and widget area that allows you to place code directly into the &lt;head&gt;. Important note: The Utility Code Widget does not display the title on the site and is capable of processing PHP. It is the only widget you should use in the <head> widget area. Widget titles in the utility widget area are hidden for all widgets in it. So you can label them for easy reference
 * Version: 1.0.1
 * Author: Milo Jennings
 * BitBucket Plugin URI: https://bitbucket.org/nucleartuxedo/wp-utility-widgets
 * BitBucket Branch: master
 */


define('UTILITY_WIDGETS_PATH', plugin_dir_path(__FILE__) );
define('UTILITY_WIDGETS_URL', plugins_url('', __FILE__ ) );
define('UTILITY_WIDGETS_ROOT_PLUGIN_URL', dirname(plugins_url('', __FILE__ )) );

add_action( 'after_setup_theme', 'register_extra_widgets', 11 );

function register_extra_widgets() {
	$domain = "wp_utility_widgets";
	$utility_area_widget = array(
		'id' => 'utility-widget-area',
		'name' => __( 'Utility Widget Area', $domain ),
		'description' => __( 'Utility Widget Area at the bottom of the page, with no wrapper markup', $domain ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<span style="display:none">',
		'after_title' => '</span>'
	);
	register_sidebar( $utility_area_widget );
	
	$extra_footer_area_widget = array(
		'id' => 'extra-footer-area',
		'name' => __( 'Extra Footer Area', $domain ),
		'description' => __( 'An extra widget area at the bottom of the page.', $domain ),
		'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-wrap widget-inside">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	);
	
	/* Register the sidebar. */
	register_sidebar( $extra_footer_area_widget );
	
	$head_area_widget = array(
		'id' => 'head-area',
		'name' => '<head>',
		'description' => 'WARNING: Only use the Code widget in this area. Normal widgets break page validation. Widgets appear directly in the <head>.',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	);
	
	/* Register the sidebar. */
	register_sidebar( $head_area_widget );
}

/* Display widget content wherever wp_footer() is executed */

if( !is_admin() ){
	add_action('wp_footer', 'add_extra_footer_widget_area');
	add_action('wp_footer', 'add_utility_widget_area');
	add_action('wp_head', 'add_head_widget_area');
	
	function add_utility_widget_area(){
		dynamic_sidebar( "utility-widget-area" );
	}
	
	function add_extra_footer_widget_area(){
		echo "<div id='extra-footer'><div id='extra-footer-wrapper'><div id='extra-footer-inside'>";
			dynamic_sidebar( "extra-footer-area" );
		echo "</div></div></div>";
	}
	function add_head_widget_area(){
		dynamic_sidebar( "head-area" );
	}
}


/* Add function to widgets_init that'll load our widget. */
add_action( 'widgets_init', 'utility_code_load_widgets' );

/* Register our widget. */
function utility_code_load_widgets() {
	register_widget( 'Utility_Code_Widget' );
}

class Utility_Code_Widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	public function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'utility-code', 'description' => __('A text widget that displays no title on the site and is capable of processing PHP', 'utility-code') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 600, 'height' => 350, 'id_base' => 'utility-code-widget' );

		/* Create the widget. */
		parent::__construct( 'utility-code-widget', __('Code', 'code'), $widget_ops, $control_ops );
	}

	/**
	 * Display the widget on the screen.
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$input = $instance['input'];

		/* Before widget (defined by themes). */
		//echo $before_widget;
		
		$text = apply_filters( 'widget_text', $instance['text'], $instance );
		
		ob_start(); 
		eval("?>$text<?php "); 
		$output = ob_get_contents(); 
		ob_end_clean(); 
		echo $output;

		/* After widget (defined by themes). */
		//echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		/* Allow tags if user has permission. */
		if ( current_user_can('unfiltered_html') )
			$instance['text'] =  $new_instance['text'];
		else
			$instance['text'] = strip_tags( $new_instance['text'] );
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	public function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => '', 'text' => '' );
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		$text = format_to_edit($instance['text']);
		
		?>
		<!-- Widget Title -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Reference Label:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat"/>
		</p>

		<!-- Widget text -->
		<p>
			<textarea id="<?php echo $this->get_field_id( 'text' ); ?>" class="widefat wp_widget_code_area" name="<?php echo $this->get_field_name( 'text' ); ?>" rows="18" columns="30" style="margin-left:0; margin-right: 0; width: 606px"><?php echo $instance['text']; ?></textarea>
		</p>

	<?php
	}
}