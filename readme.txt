## WP Utility Widgets ##

Contributors: Milo Jennings
Tags: 
Requires at least: 3.0
Tested up to: 4.2.2
Version: 1.0.1
Stable tag: 1.0.1


### Description ###

Adds a widget type labeled "Code". Registers 3 new widget areas. A footer, a utility area that's placed in the footer and widget area that allows you to place code directly into the &lt;head&gt;. Important note: The Utility Code Widget does not display the title on the site and is capable of processing PHP. It is the only widget you should use in the &lt;head&gt; widget area. Widget titles in the utility widget area are hidden for all widgets in it. So you can label them for easy reference


* Adds a widget type labeled "Code". This widget is capable of processing php, and does not display a title on the site. You can use the title to label your widget for easy reference.
* The plugin also registers two new widget areas. The content will appear on every page at the bottom of the markup, wherever the wp_footer() function is executed in the template file
* The utility widget area is configured to hide the widget title, so you can use the title field in the admin interface to label your widgets for easy reference. The titles will no appear on the page for visitors, but are displayed in the markup if you view the page source.