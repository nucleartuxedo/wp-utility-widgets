== Changelog ==

Version 1.0.1 [04/17/2018]

- Updated deprecated widget code to work with WordPress 4.3.0 and newer.
- Fixed a missing variable for string translation.

Version 1.0.0 [06/22/2015]

- Removed Quark framework references for auto updating
- Connected up with Bitbucket to use with github auto updater plugin.
- Added syntax highlighting.

Version 0.3 [10/31/2011]

- Removed wpautop filtering, so there are no &lt;p&gt; tags automatically added for new lines.

Version 0.2 [10/25/2011]

- Added widget area &lt;head&gt;, which allows widgets to be placed directly into the head element.
- Renamed widget from "Utility Text Widget" to "Code", so the title has more room for the reference label.

Version 0.1 [10/25/2011]

- Created plugin